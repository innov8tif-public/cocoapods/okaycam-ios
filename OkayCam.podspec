Pod::Spec.new do |s|
    s.name             = 'OkayCam'
    s.version          = '2.0.15'
    s.license          = { :file => 'LICENSE', :type => 'Commercial' }
    s.author           = { 'Innov8tif' => 'ekyc.team@innov8tif.com' }
    s.homepage         = 'https://innov8tif.com'
    s.summary          = 'OkayCam iOS SDK.'
    s.source           = { :git => 'https://gitlab.com/innov8tif-ekyc-product/cocoapods/okaycam-ios', :tag => 'v2.0.15', :branch => 'master' }

    s.pod_target_xcconfig = { 
   		'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
   		'IPHONEOS_DEPLOYMENT_TARGET' => '15.0' 
 	}
    s.user_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
    
    s.ios.deployment_target = '15.0'
    s.vendored_frameworks = 'OkayCam.xcframework'
    
    s.swift_version = '5.0'
    s.dependency 'CryptoSwift', '1.8.2'
end
  
